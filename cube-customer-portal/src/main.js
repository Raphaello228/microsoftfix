import { createApp } from 'vue'
import { UserManager } from 'oidc-client';
import { PublicClientApplication } from "@azure/msal-browser";
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'

loadFonts()

class MsalUserManager {
    constructor(msalApplication, loginRequest) {
        this.msalApplication = msalApplication;
        this.loginRequest = loginRequest;
    }

    async signIn() {
        const loginResponse = await this.msalApplication.loginPopup(this.loginRequest);
        return loginResponse.account;
    }

    getUser() {
        const accounts = this.msalApplication.getAllAccounts();
        if (accounts.length > 0) {
            return Promise.resolve(accounts[0]);
        }
        return Promise.resolve(null);
    }
}

const googleConfig = {
    authority: "https://accounts.google.com",
    client_id: "629474493874-et2achjnbfmf7fdgn4pgasfkoai33nlk.apps.googleusercontent.com",
    client_secret: "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",
    redirect_uri: "http://localhost:5173",
    response_type: "code",
    scope: "openid profile email"
};

const microsoftConfig = {
    auth: {
        clientId: "dd58c02e-f53c-4c17-ba26-e33a9f62fd4f",
        authority: "https://login.microsoftonline.com/f8cdef31-a31e-4b4a-93e4-5f571e91255a",
    }
};

const googleUserManager = new UserManager(googleConfig);

const pca = new PublicClientApplication(microsoftConfig);
const microsoftUserManager = new MsalUserManager(pca, {
    scopes: ["user.read"],
    redirectUri: "http://localhost:5173",
});

createApp(App)
    .use(router)
    .use(vuetify)
    .provide('googleUserManager', googleUserManager)
    .provide('microsoftUserManager', microsoftUserManager)
    .mount('#app')
